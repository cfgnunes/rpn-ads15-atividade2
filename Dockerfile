FROM nginx

# Define os labels em uma linha
LABEL maintainer="Cristiano" \
      vendor="Cristiano" \
      br.com.cfgnunes.version="1.0.0" \
      br.com.cfgnunes.build-date="2020-11-27" \
      br.com.cfgnunes.release-date="2020-11-27"

ARG VARIAVEL="Cristiano Nunes"

COPY index.html /usr/share/nginx/html/

RUN sed -i "s|Hello World|Oi $VARIAVEL|g" /usr/share/nginx/html/index.html
